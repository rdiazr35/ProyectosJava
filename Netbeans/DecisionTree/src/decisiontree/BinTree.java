/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decisiontree;

/**
 *
 * @author Randy
 */
public class BinTree {
    
    /* FIELDS */
	
    private int     nodeID;
    private String  questOrAns = null;
    private BinTree yesBranch  = null;
    private BinTree noBranch   = null;

    /* CONSTRUCTOR */

    public BinTree(int newNodeID, String newQuestAns) {
        nodeID     = newNodeID;
        questOrAns = newQuestAns;
    }
	
}
