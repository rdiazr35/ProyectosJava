/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafos;

import javax.swing.JTextArea;

/**
 *
 * @author dennisvalverde
 */
public class Grafo {
    
    private Vertice primero;

    public Grafo() {
        this.primero = null;
    }
    
    
    public void insertarVertice(String nombre){
        
        if (primero == null){
            primero = new Vertice(nombre);
        }
        
        else {
            
            Vertice temp = primero;
            
            while(temp.getSig() != null){
                temp = temp.getSig();
            }
            
            Vertice nn = new Vertice(nombre);
            temp.setSig(nn);
        }
        
    }
    
    void insertarArco(String origen, String destino, int peso){
        
        Vertice tempO = primero;
        
        while(tempO != null){
            
            if(tempO.getNombre().equals(origen)){
                
                Arco nuevoArco = new Arco(peso);
                
                Vertice tempD = primero;
                
                while(tempD != null){
                    
                    if (tempD.getNombre().equals(destino)){    
                        nuevoArco.setDestino(tempD);   
                    }
                    
                    tempD = tempD.getSig();
                    
                }
                
                if (tempO.getPrimerArco() == null){
                    tempO.setPrimerArco(nuevoArco);
                }
                
                else {
                    nuevoArco.setSig(tempO.getPrimerArco());
                    tempO.setPrimerArco(nuevoArco);
                }
                   
            }
            tempO = tempO.getSig();
        }
        
    }
    
    void imprimirAmplitud(JTextArea textArea){
        
        textArea.setText("");
        
        if (primero == null){
            textArea.append("Grafo vacío.");
        }
        
        else {
            
            Vertice temp = primero;
            
            while (temp != null){
                
                Arco tempA = temp.getPrimerArco();
                
                while(tempA != null){
                    textArea.append(temp.getNombre() + '-' 
                            + tempA.getDestino().getNombre() + " "  +
                            tempA.getPeso() +"\n");
                    tempA = tempA.getSig();
                }
                
                temp = temp.getSig();
            }
            
            
        }
        
    }
    
    
}
