/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafos;

/**
 *
 * @author dennisvalverde
 */
public class Arco {
    
    private int peso;
    private Vertice destino;
    private Arco sig;

    public Arco(int peso) {
        this.peso = peso;
        this.destino = null;
        this.sig = null;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public Vertice getDestino() {
        return destino;
    }

    public void setDestino(Vertice destino) {
        this.destino = destino;
    }

    public Arco getSig() {
        return sig;
    }

    public void setSig(Arco sig) {
        this.sig = sig;
    }
       
}

