/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafos;

/**
 *
 * @author dennisvalverde
 */
public class Vertice {
    
    private String nombre;
    private Arco primerArco;
    private Vertice sig;

    public Vertice(String nombre) {
        this.nombre = nombre;
        this.primerArco = null;
        this.sig = null;
    }

    public Arco getPrimerArco() {
        return primerArco;
    }

    public void setPrimerArco(Arco primerArco) {
        this.primerArco = primerArco;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Vertice getSig() {
        return sig;
    }

    public void setSig(Vertice sig) {
        this.sig = sig;
    }
    
    
    
}
