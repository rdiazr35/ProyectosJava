package lab2;

public class Lab2 {
    
    public static Arbol oArbol;
    public static int primerArbol;
    public static int segundoArbol;
    
    public static void main(String[] args) {
        // TODO code application logic here
        Proceso();
    }
    //
    public static void Proceso(){
        oArbol = new Arbol();
        oArbol.insertar(3);
        oArbol.insertar(5);
        oArbol.insertar(7);
        oArbol.insertar(1);
        oArbol.insertar(0);

        System.out.println("Arbol #1 ======================== ");
        oArbol.imprimirEntre();
        
        oArbol.insertarArbol2(3);
        oArbol.insertarArbol2(5);
        oArbol.insertarArbol2(7);
        oArbol.insertarArbol2(1);
        oArbol.insertarArbol2(0);
        
        System.out.println("Arbol #2 ======================== ");
        oArbol.imprimirEntreArbol2();
        
        //System.out.println("Impresión pre orden: ");
        //oArbol.imprimirPre();
        //System.out.println("Impresión entre orden: ");
        //oArbol.imprimirEntre();
        //System.out.println("Impresión post orden: ");
        //oArbol.imprimirPost();
        
        primerArbol = oArbol.contarArbol();
        System.out.println(primerArbol);
        segundoArbol = oArbol.contarArbol2();
        System.out.println(segundoArbol);
        PocentejeExactitud(oArbol);
        //PorcentajeSimilitud(oArbol);
    }
    //
    public static void PorcentajeSimilitud(Arbol oArbol){
        if (primerArbol == segundoArbol) {
            //primerArbol
            System.out.println(oArbol.iguales(primerArbol));
        }
        else if (primerArbol > segundoArbol) {
            //primerArbol
            System.out.println(oArbol.iguales(primerArbol));
        }
        else{
            //segundoArbol
            System.out.println(oArbol.iguales(segundoArbol));
        }
    }
    //
    public static void PocentejeExactitud(Arbol oArbol){
        if (primerArbol == segundoArbol) {
            //primerArbol
            double iguales = (double)oArbol.iguales(primerArbol);
            double porc = (iguales / (double)primerArbol) * 100;
            System.out.println(porc);
        }
        else if (primerArbol > segundoArbol) {
            //primerArbol
            double iguales = (double)oArbol.iguales(primerArbol);
            double porc = (iguales / (double)primerArbol) * 100;
            System.out.println(porc);
        }
        else{
            //segundoArbol
            double iguales = (double)oArbol.iguales(segundoArbol);
            double porc = (iguales / (double)segundoArbol) * 100;
            System.out.println(porc);
        }
    }
    //
}