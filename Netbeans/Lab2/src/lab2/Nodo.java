package lab2;

/**
 *
 * @author Randy
 */
public class Nodo {
    private int info;
    private Nodo izq;    
    private Nodo der;
    
    public Nodo() {
    
    }
    //
    public Nodo(int numero) {
        this.info = numero;
        this.izq = null;
        this.der = null;
    }
    //
    public Nodo getIzq() {
        return izq;
    }
    //
    public void setIzq(Nodo izq) {
        this.izq = izq;
    }
    //
    public Nodo getDer() {
        return der;
    }
    //
    public void setDer(Nodo der) {
        this.der = der;
    }
    //
    public int getInfo() {
        return info;
    }
    //
    public void setInfo(int info) {
        this.info = info;
    }
    //
    public void insertar(int num){
        if(this.info >= num){
            if(this.izq == null){
                this.izq = new Nodo(num);
            }
            else {
                this.izq.insertar(num);
            }
        }else {
            if(this.der == null){
                this.der = new Nodo(num);
            }else {
                this.der.insertar(num);
            }
        }
    }
    //
}//END