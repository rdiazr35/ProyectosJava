/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

/**
 *
 * @author Randy
 */
public class Arbol {
    Nodo raiz;
    Nodo raiz2;
    int conta= 0;

    public Arbol(){
    }
    //
    public void insertar(int info) {
        Nodo nuevo;
        nuevo = new Nodo();
        nuevo.setInfo(info);
        nuevo.setIzq(null);
        nuevo.setDer(null);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            Nodo anterior = null, reco;
            reco = raiz;
            while (reco != null) {
                anterior = reco;
                if (info < reco.getInfo()) {
                    reco = reco.getIzq();
                } else {
                    reco = reco.getDer();
                }
            }
            if (info < anterior.getInfo()) {
                anterior.setIzq(nuevo);
            } else {
                anterior.setDer(nuevo);
            }
        }
    }
    //
    public void insertarArbol2(int info) {
        Nodo nuevo;
        nuevo = new Nodo();
        nuevo.setInfo(info);
        nuevo.setIzq(null);
        nuevo.setDer(null);
        if (raiz2 == null) {
            raiz2 = nuevo;
        } else {
            Nodo anterior = null, reco;
            reco = raiz2;
            while (reco != null) {
                anterior = reco;
                if (info < reco.getInfo()) {
                    reco = reco.getIzq();
                } else {
                    reco = reco.getDer();
                }
            }
            if (info < anterior.getInfo()) {
                anterior.setIzq(nuevo);
            } else {
                anterior.setDer(nuevo);
            }
        }
    }
    //
    private void imprimirPre(Nodo reco) {
        if (reco != null) {
            System.out.print(reco.getInfo() + " ");
            imprimirPre(reco.getIzq());
            imprimirPre(reco.getDer());
        }
    }
    //
    public void imprimirPre() {
        imprimirPre(raiz);
        System.out.println();
    }
    //
    private void imprimirEntre(Nodo reco) {
        if (reco != null) {
            imprimirEntre(reco.getIzq());
            System.out.print(reco.getInfo() + " ");
            imprimirEntre(reco.getDer());
        }
    }
    //
    public void imprimirEntre() {
        imprimirEntre(raiz);
        System.out.println();
    }
    //
    public void imprimirEntreArbol2() {
        imprimirEntre(raiz2);
        System.out.println();
    }
    //
    private void imprimirPost(Nodo reco) {
        if (reco != null) {
            imprimirPost(reco.getIzq());
            imprimirPost(reco.getDer());
            System.out.print(reco.getInfo() + " ");
        }
    }
    //
    public void imprimirPost() {
        imprimirPost(raiz);
        System.out.println();
    }
    //
    //=========================================================
    //
    public void agregarNodo(int numero){
        
        if(raiz == null){
              raiz = new Nodo(numero);         
        }
        
        else {
            raiz.insertar(numero);
        }  
        
    }
    //
    public void imprimir(){
        if(raiz == null){
            System.out.println("Arbol vacío");
        }
        else {
            imprimirAux(raiz);
        }
    }
    //
    private void imprimirAux(Nodo temp){
        
        if(temp != null){
            imprimirAux(temp.getIzq());
            System.out.println(temp.getInfo());
            imprimirAux(temp.getDer());
        }
        
    }
    //
    public int contarArbol(){
        if(raiz == null){
            return 0;
        }
        else {
            return contarArbolAux(raiz);
        }
    }
    //
    public int contarArbol2(){
        if(raiz2 == null){
            return 0;
        }
        else {
            return contarArbolAux(raiz2);
        }
    }
    //
    private int contarArbolAux(Nodo nodo){
        if(nodo == null){
            return 0;
        }
        else {
            return 1 + contarArbolAux(nodo.getIzq()) + contarArbolAux(nodo.getDer());
        }
    }
    //
    private void exactitud(Nodo reco, Nodo reco2) {
        if (reco != null && reco2 != null) {
            exactitud(reco.getIzq(), reco2.getIzq());
            if (reco.getInfo() == reco2.getInfo()) {
                conta++;
            }
            exactitud(reco.getDer(), reco2.getDer());
        }
    }
    //
    public int iguales(int cantidad){
        exactitud(raiz, raiz2);
        return conta;
    }
    private void similitud(Nodo reco, Nodo reco2) {
        if (reco != null && reco2 != null) {
            similitud(reco.getIzq(), reco2.getIzq());
            if (reco.getInfo() == reco2.getInfo()) {
                conta++;
            }
            similitud(reco.getDer(), reco2.getDer());
        }
    }
    //
    public int similitud(int cantidad){
        similitud(raiz, raiz2);
        return conta;
    }
}