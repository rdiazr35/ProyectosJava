package arbolbinarioexample;

/**
 *
 * @author Randy
 */
public class Principal {

    public static void main(String[] args) {
        // TODO code application logic here
        ArbolBinario oArbol = new ArbolBinario();
        oArbol.insertar(8);
        oArbol.insertar(6);
        oArbol.insertar(1);
        oArbol.insertar(4);
        oArbol.insertar(5);
        oArbol.insertar(3);
        oArbol.insertar(7);
        oArbol.insertar(2);
        oArbol.insertar(9);
        oArbol.insertar(10);
        
        System.out.println("Impresión pre orden: ");
        oArbol.imprimirPre();
        System.out.println("Impresión entre orden: ");
        oArbol.imprimirEntre();
        System.out.println("Impresión post orden: ");
        oArbol.imprimirPost();
        
    }
    
}
