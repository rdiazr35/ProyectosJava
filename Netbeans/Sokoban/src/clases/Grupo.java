/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Excepciones.ExcepcionGrupo;
import clasesUsuario.Jugador;
import java.util.ArrayList;

/**
 *
 * @author Administrador
 */
public class Grupo {

    //jugadores del grupo
    private ArrayList<Jugador> jugadores;
    //niveles del grupo
    private ArrayList<Nivel> niveles;
    //jugador que esta jugan
    private Jugador jugadorActual;
    //nivel que se esta jugando
    private Nivel nivelActual;

    /**
     * Constructor de la clase Grupo
     *
     * @param jugadores Lista de jugadores que van a conformar el grupo
     * @param niveles Lista de niveles que conforman el grupo
     * @throws Excepciones.ExcepcionGrupo
     */
    public Grupo(ArrayList<Jugador> jugadores, ArrayList<Nivel> niveles) throws ExcepcionGrupo {
        this.setJugadores(jugadores);
        this.setNiveles(niveles);

        this.jugadorActual = jugadores.get(0);
        sokoban.Sokoban.setUsuarioActual(jugadorActual);

        this.nivelActual = null;
    }

    /**
     *
     * @return Obtiene el listado de Jugadores
     */
    public ArrayList<Jugador> getJugadores() {
        return jugadores;
    }

    /**
     *
     * @param jugadores Establece el listado de Jugadores
     * @throws Excepciones.ExcepcionGrupo
     */
    public void setJugadores(ArrayList<Jugador> jugadores) throws ExcepcionGrupo {
        if ((jugadores.size() < 2) || (jugadores.size() > 4)) {
            throw new ExcepcionGrupo("La cantidad de jugadores en un grupo deben ser un mínimo 2 y máximo de 4");
        }
        this.jugadores = jugadores;
    }

    /**
     *
     * @return Obtiene el listado de niveles
     */
    public ArrayList<Nivel> getNiveles() {
        return niveles;
    }

    /**
     *
     * @param niveles Establece el listado de niveles
     * @throws Excepciones.ExcepcionGrupo
     */
    public void setNiveles(ArrayList<Nivel> niveles) throws ExcepcionGrupo {
        if ((niveles.size() < 1) || (niveles.size() > 5)) {
            throw new ExcepcionGrupo("La cantidad de niveles en un grupo deben de ser mínimo 1 y máximo 5");
        }
        this.niveles = niveles;
    }

    public Nivel getNivelActual() {
        return nivelActual;
    }

    public void setNivelActual(Nivel nivelActual) {
        this.nivelActual = nivelActual;
    }

    @Override
    public String toString() {
        return "Grupo{" + "jugadores=" + jugadores + ", niveles=" + niveles + ", jugadorActual=" + jugadorActual + ", indiceNivelActual=" + nivelActual + '}';
    }

    /**
     * Inicia el juego para un Grupo
     */
    public boolean jugarGrupo() throws Exception {
        Nivel sigNivel = siguienteNivel();
        if (sigNivel != null) {
            if((jugadorActual.getJuegoActual() != null)
                && (jugadorActual.getJuegoActual().getGrupo().equals(this)))
            {
              jugadorActual.getJuegoActual().siguienteNivel();
            }
            else
              jugadorActual.crearNuevoJuego(this);
            
            return true;
        }
        return false;
    }

    private Nivel siguienteNivel() {
        if ((jugadores.size() > 0) && (niveles.size() > 0)) {

            if ((niveles.indexOf(nivelActual) + 1) < niveles.size()) {
                boolean lAbandono = false;

                if (jugadorActual != null) {
                    //se verifica que no hubiera abandonado el grupo
                    //perdiendo la partida en la que estaba en el grupo
                    for (Juego juego : jugadorActual.getJuegos()) {
                        if ((juego.getGrupo().equals(this)) && (!juego.getEstado().equals("En Proceso"))) {
                            lAbandono = true;
                            break;
                        }
                    }
                }
                nivelActual = niveles.get(niveles.indexOf(nivelActual) + 1);
                if (!lAbandono) {
                    return nivelActual;
                } else {
                    return siguienteNivel();
                }
            }
            
            if (jugadores.indexOf(jugadorActual) + 1 < jugadores.size()) {
                jugadorActual = jugadores.get(jugadores.indexOf(jugadorActual) + 1);
                sokoban.Sokoban.setUsuarioActual(jugadorActual);
                nivelActual = null;
                return siguienteNivel();
            }
        }

        return null;
    }
    
    public void verificarJuegoGanadoJugadorActual(){
        if((niveles.size()>0)&&(niveles.indexOf(nivelActual) == niveles.size()-1)){
            jugadorActual.getJuegoActual().setEstado("Ganado");
        }
    }

    public ArrayList<ArrayList<Object>> obtenerResultadosFinales() {
        ArrayList<ArrayList<Object>> resultadosGrupo = new ArrayList<>();
        
        for (Jugador jugador : jugadores) {
            Juego juegoGrupo = null;
            for (Juego juego : jugador.getJuegos()) {
              if(juego.getGrupo().equals(this)){
                  juegoGrupo = juego;
                  break;
              }
            }
            
            if(juegoGrupo != null){
               ArrayList<Object> infoResultado = new ArrayList<>();
               infoResultado.add(jugador);
               int lNivelMaximoGanado = 0;
               int lCantMovTotal = 0;
               int lTiempoTotal = 0;
                for (JuegoNivel jn : juegoGrupo.getNivelesJugador()) {
                   if((jn.getEstado().contains("Ganado")) 
                       && (jn.getNivel().getNumeroNivel() > lNivelMaximoGanado)){
                       lNivelMaximoGanado = jn.getNivel().getNumeroNivel();
                   } 
                   lCantMovTotal+=jn.getCantMovimientos();
                   lTiempoTotal+=jn.getCantSegundos();
                }
                infoResultado.add(lNivelMaximoGanado);
                infoResultado.add(lCantMovTotal);
                infoResultado.add(lTiempoTotal);
                resultadosGrupo.add(infoResultado);
            }
        }
        return resultadosGrupo;
    }
}
