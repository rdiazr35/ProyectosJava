/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Excepciones.ExcepcionNivel;
import Excepciones.ExcepcionUsuario;
import clasesEspacio.Caja;
import clasesEspacio.CajaPunto;
import clasesEspacio.Disponible;
import clasesEspacio.Pared;
import clasesEspacio.Personaje;
import clasesEspacio.Punto;
import clasesUsuario.Jugador;
import interfaz.JugarNivel;
import java.util.ArrayList;
import superClases.Espacio;

/**
 * Clase en la cual se lleva el registro de los niveles y sus estados de un
 * juego
 *
 * @author Administrador
 */
public class JuegoNivel {

    //nivel al que se corresponde
    private Nivel nivel;
    //estado en que se encuentra el juego
    private ArrayList<ArrayList<Espacio>> estadoActualNivel;
    //estado del juego 1 jugada atras
    private ArrayList<ArrayList<Espacio>> estadoAnteriorNivel;
    //estado del juego 2 jugadas atras
    private ArrayList<ArrayList<Espacio>> estadoTrasAnteriorNivel;
    private ArrayList<ArrayList<Espacio>> reespaldoMatrizActual;
    //cantidad de movimientos que se han realizado
    private int cantMovimientos;

  
    //cantidad de segundos que lleva el juego
    private int cantSegundos;
    //cantidad de veces que se han hecho reversiones, solo se puede un máximo de 2
    private int cantReversiones;
    /*
    Los estados de un Juego seran:  
    En Proceso
    Ganado
    Ganado Optimizado
    Perdido
     */
    private String estado;
    //cantidad de puntos que faltan para ganar el juego
    private int cantPuntosRestantes;
    private Personaje personaje;

    /**
     * Constructor de la clase JuegoNivel Clase en la cual se lleva el historial
     * de niveles jugados por juego
     *
     * @param nivel
     * @throws java.lang.Exception
     */
    public JuegoNivel(Nivel nivel) throws Exception {
        this.nivel = nivel;
        this.cantMovimientos = 0;
        this.cantPuntosRestantes = 0;
        this.cantSegundos = 0;
        this.cantReversiones = 0;
        this.estado = "En Proceso";
        this.estadoActualNivel = new ArrayList<>();
        this.estadoAnteriorNivel = null;
        this.estadoTrasAnteriorNivel = null;
        generarEstadoInicialNivel();
    }

    /**
     *
     * @return Retorna el nivel correspondiente
     */
    public Nivel getNivel() {
        return nivel;
    }

    /**
     *
     * @return Retorna el estado actual de las posiciones de los diferentes
     * campos
     */
    public ArrayList<ArrayList<Espacio>> getEstadoActualNivel() {
        return estadoActualNivel;
    }

    /**
     *
     * @return
     */
    public Personaje getPersonaje() {
        return personaje;
    }

    /**
     *
     * @return Retorna la cantdad de Movimientos realizados
     */
    public int getCantMovimientos() {
        return cantMovimientos;
    }

    /**
     *
     * @return Retorna la cantidad de segundos jugados
     */
    public int getCantSegundos() {
        return cantSegundos;
    }

    /**
     *
     * @return Retorna el estado del Juego
     */
    public String getEstado() {
        return estado;
    }  
    
    /**
     *
     */
    public void setEstadoGanado(){
        this.estado = "Ganado";
    }

    /**
     *
     * @return
     */
    public int getCantReversiones() {
        return cantReversiones;
    }
    

    /**
     *
     * @param estadoActualNivel
     */
    public void setEstadoActualNivel(ArrayList<ArrayList<Espacio>> estadoActualNivel) {
        this.estadoActualNivel = estadoActualNivel;
    }
    
    /**
     *
     * @param cantMovimientos
     */
    public void setCantMovimientos(int cantMovimientos) {
        this.cantMovimientos = cantMovimientos;
    }

    /**
     *
     * @param cantSegundos
     */
    public void setCantSegundos(int cantSegundos) {
        this.cantSegundos = cantSegundos;
    }
     
    /**
     *
     * @return
     */
    public boolean puedeReversar()
    {
        return (this.cantMovimientos > 1) && (sokoban.Sokoban.getConfiguracion().isPermitirDeshacer())
                &&(this.cantReversiones < 2) && (this.estadoAnteriorNivel != null);
    }
      

    @Override
    public String toString() {
        return "JuegoNivel{" + "nivel=" + nivel + ", estadoActualNivel=" + estadoActualNivel + ", estadoAnteriorNivel=" + estadoAnteriorNivel + ", estadoTrasAnteriorNivel=" + estadoTrasAnteriorNivel + ", cantMovimientos=" + cantMovimientos + ", cantSegundos=" + cantSegundos + ", cantReversiones=" + cantReversiones + ", estado=" + estado + ", cantPuntosRestantes=" + cantPuntosRestantes + '}';
    }

    /*
    * Genera el estado de la matriz inicial de nivel, este lo obtiene de la matriz del nivel
    *
     */
    private void generarEstadoInicialNivel() throws Exception {
        for (ArrayList<Espacio> fila : this.nivel.getEstadoInicial()) {
            ArrayList<Espacio> cFila = new ArrayList<>();
            for (Espacio espacio : fila) {
                Espacio cEspacio = null;
                if (espacio instanceof Caja) {
                    cEspacio = new Caja(espacio.getX(), espacio.getY());
                } else if (espacio instanceof Disponible) {
                    cEspacio = new Disponible(espacio.getX(), espacio.getY());
                } else if (espacio instanceof Pared) {
                    cEspacio = new Pared(espacio.getX(), espacio.getY());
                } else if (espacio instanceof Personaje) {
                    cEspacio = new Personaje(espacio.getX(), espacio.getY());
                    this.personaje = (Personaje) cEspacio;
                } else if (espacio instanceof Punto) {
                    this.cantPuntosRestantes++;
                    cEspacio = new Punto(espacio.getX(), espacio.getY());
                }
                cFila.add(cEspacio);
            }
            this.estadoActualNivel.add(cFila);
        }

    }
    
    /**
     *
     * @throws Exception
     */
    public void reespaldarMatrizActual() throws Exception{
       this.reespaldoMatrizActual = new ArrayList<>();
        for (ArrayList<Espacio> filas : estadoActualNivel) {
            ArrayList<Espacio> rFila = new ArrayList<>();
            for (Espacio columna : filas) {
              Espacio rEspacio = null;
              if(columna instanceof Caja)
                  rEspacio = new Caja(columna.getX(), columna.getY());
              else if(columna instanceof CajaPunto)
                  rEspacio = new CajaPunto(columna.getX(), columna.getY());
              else if(columna instanceof Disponible)
                  rEspacio = new Disponible(columna.getX(), columna.getY());
              else if(columna instanceof Pared)
                  rEspacio = new Pared(columna.getX(), columna.getY());
              else if(columna instanceof Personaje)
                  rEspacio = new Personaje(columna.getX(), columna.getY());
              else if(columna instanceof Punto)
                  rEspacio = new Punto(columna.getX(), columna.getY());
              rFila.add(rEspacio);
            }
            this.reespaldoMatrizActual.add(rFila);            
        }
    }
    
    /**
     *
     */
    public void guardarMovAnterior(){
        this.estadoTrasAnteriorNivel = this.estadoAnteriorNivel;
        this.estadoAnteriorNivel = this.reespaldoMatrizActual;
    }

    /*
    * Valida si el movimiento que se desea hacer es valido
    * 
     */

    /**
     *
     * @param tipoObjetoMov
     * @param pDireccion
     * @param pCantIteraciones
     * @return
     * @throws Exception
     */

    public Object[] validarMovimiento(Espacio tipoObjetoMov, String pDireccion,int pCantIteraciones) throws Exception {
        int x = 0, y = 0;
        boolean lMovCaja = false;
        

        if (pDireccion.equals("arriba")) {
            x = -1;
        } else if (pDireccion.equals("derecha")) {
            y = 1;
        } else if (pDireccion.equals("abajo")) {
            x = 1;
        } else if (pDireccion.equals("izquierda")) {
            y = -1;
        }
        if (((tipoObjetoMov.getX() + x) < 0) || ((tipoObjetoMov.getY() + y) < 0)) {
            Object[] returnValue = {false,null};
            return returnValue;
        } else {
            Espacio nuevaPos = this.estadoActualNivel.get((tipoObjetoMov.getX() + x)).get((tipoObjetoMov.getY() + y));
            if (!nuevaPos.isBloquea()) {
                return mover(tipoObjetoMov, (tipoObjetoMov.getX() + x), (tipoObjetoMov.getY() + y),new Object[]{true,tipoObjetoMov});
            }
            else if((nuevaPos.isDesplaza()) && 
                    (pCantIteraciones < sokoban.Sokoban.getConfiguracion().getCajasPorMovimiento())){
                Object[] returnValue = validarMovimiento(nuevaPos, pDireccion, (pCantIteraciones+1));
                boolean movValido = Boolean.valueOf(returnValue[0].toString()); 
                
                if (movValido)
                   return mover(tipoObjetoMov, (tipoObjetoMov.getX() + x), (tipoObjetoMov.getY() + y),returnValue);
            }
        }
        
        Object[] returnValue = {false,null};
        return returnValue;
    }

    /**
     *
     *
     * @param espacio
     * @param pNuevoX
     * @param pNuevo
     *
     * @return Un mensaje en el cual se indicará si existe algun inconveniente o
     * si el movimiento fue realizado con exito
     */
    private Object[] mover(Espacio espacio, int pNuevoX, int pNuevoY,Object[] pResult) throws Exception {

        /*se coloca el espacio en el nuevo lugar indicado*/
        Espacio espMov = this.nivel.getEstadoInicial().get(pNuevoX).get(pNuevoY);
        Espacio espOriginal = this.nivel.getEstadoInicial().get(espacio.getX()).get(espacio.getY());
        
        /*en caso que se mueva una caja y cae en un puntose tiene que cambiar el tipo de objeto*/
        if(((espacio instanceof Caja)||(espacio instanceof CajaPunto)) && (espMov instanceof Punto))
        {
          if(espacio instanceof Caja) 
              this.cantPuntosRestantes--;
          
          espacio = new CajaPunto(espOriginal.getX(), espOriginal.getY());
        }
        
        if((espacio instanceof CajaPunto) && (espMov instanceof Disponible))
        {
            this.cantPuntosRestantes++;
          espacio = new Caja(espOriginal.getX(), espOriginal.getY());
        }
            
        this.estadoActualNivel.get(pNuevoX).set(pNuevoY, espacio);
        
        
        Espacio espRem = null;
        if ((espOriginal instanceof Disponible) || (espOriginal instanceof Caja)) {
            espRem = new Disponible(espOriginal.getX(), espOriginal.getY());
        } else if (espOriginal instanceof Personaje) {
            espRem = new Disponible(espOriginal.getX(), espOriginal.getY());
        } 
        else if (espOriginal instanceof Punto) {
            espRem = new Punto(espOriginal.getX(), espOriginal.getY());
        }
        
        

        /*reemplaza la antigua ubicación por el original*/
        this.estadoActualNivel.get(espOriginal.getX()).set(espOriginal.getY(), espRem);

        espacio.setX(pNuevoX);
        espacio.setY(pNuevoY);
        Object[] returnValue = null;
        
        if(espacio instanceof Personaje){
            
            returnValue = pResult;         
        }
        else{
            returnValue = new Object[2];
            returnValue[0] = true;
            returnValue[1] = espacio;                    
        }
        return returnValue;
        //return true;
    }

    /*
    * Retorna un valor booleano el cual indicará si el juego ha sido ganado
     */

    /**
     *
     * @return
     * @throws ExcepcionNivel
     * @throws ExcepcionUsuario
     */

    public boolean verificarNivelGanado() throws ExcepcionNivel, ExcepcionUsuario {
        if(this.cantPuntosRestantes==0){
            if(this.cantMovimientos < this.nivel.getBestMoves())
            {
               this.estado = "Ganado Optimizado";
               ((Jugador)sokoban.Sokoban.getUsuarioActual()).setRompioRecord(true);
               this.getNivel().getUsuarioAdministrador().setCantBestMovesSuperados(this.getNivel().getUsuarioAdministrador().getCantBestMovesSuperados() + 1);
               sokoban.Sokoban.cambiarBestMoves(this);
            }
            else this.estado = "Ganado";
        return true;
        }
        return false;
    }

    /**
     * Reversa una jugada atras
     *
     * @return Retorna True en caso de que la jugada pudo ser reversada
     */
    public boolean reversarJugada() {
        this.estadoActualNivel = this.estadoAnteriorNivel;
        this.estadoAnteriorNivel = this.estadoTrasAnteriorNivel;
        this.estadoTrasAnteriorNivel = null;
        this.cantReversiones++;
        return true;
    }
    
    /**
     *
     */
    public void setEstadoPerdido(){
        this.estado = "Perdido";
    }

    /**
     * Inicia el juego para el nivel indicado
     */
    public void iniciarJuegoNivel() {
        
           JugarNivel interfazJuego = new JugarNivel();
        
           interfazJuego.setVisible(true);
    }
}
