/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 * Configuración del Juego
 * 
 * @author Administrador
 */
public class Configuracion {
  
    private int cajasPorMovimiento;//la cantidad de cajas por movimiento, solo puede ser 1 ó 2
    private boolean permitirDeshacer;//indica permite el deshacer 2 jugadas por nivel

    /**
     * Generación de la Configuración del Juego
     * @param cajasPorMovimiento Indica la cantidad de cajas que se pueden mover en un solo movimiento. 
     * Esta restringido a 1 ó 2
     * @param permitirDeshacer Indica si el usuario podra realizar una reversión de su ultimo movimiento(Ctr + Z).
     */
    public Configuracion(int cajasPorMovimiento, boolean permitirDeshacer) {
        this.cajasPorMovimiento = cajasPorMovimiento;
        this.permitirDeshacer = permitirDeshacer;
    }

    /**
     *Obtener la cantidad de cajas que se pueden mover por movimiento.
     * @return Cantidad de cajas que pueden ser movidas en un solo movimiento. 
     * Restringuida a 1 ó 2.
     */
    public int getCajasPorMovimiento() {
        return cajasPorMovimiento;
    }

    /**
     * Establecer el valor para la cantidad de cajas que se pueden mover por movimiento
     * @param cajasPorMovimiento Cantidad de cajas permitidas a mover por cada movimiento.
     * @throws Exception Genera una excepción si cajasPorMovimiento es diferente a 1 ó 2
     */
    public void setCajasPorMovimiento(int cajasPorMovimiento) throws Exception {
        if ((cajasPorMovimiento >= 1) && (cajasPorMovimiento <= 2)) {
            this.cajasPorMovimiento = cajasPorMovimiento;
        } else {
            throw new Exception("La cantidad de cajas permitidas para mover por movimiento solo pueden ser 1 ó 2!!!");
        }
    }

    /**
     * Obtiene el valor para saber si es posible deshacer jugadas.
     * @return Retorna el valor de Permitir Deshacer.
     */
    public boolean isPermitirDeshacer() {
        return permitirDeshacer;
    }

    /**
     * Establece el valor para permitir deshacer jugadas
     * @param permitirDeshacer Indica si es posible deshacer jugadas.
     */
    public void setPermitirDeshacer(boolean permitirDeshacer) {
        this.permitirDeshacer = permitirDeshacer;
    }

    @Override
    public String toString() {
        return "Configuracion{" + "cajasPorMovimiento=" + cajasPorMovimiento + ", permitirDeshacer=" + permitirDeshacer + '}';
    }

}
