/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesEspacio;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import superClases.Espacio;

/**
 *Clase para el tipo de espacio Pared.
 * @author Administrador
 */
public class Pared extends Espacio{
    
    /**
     * Constructor de la clase Pared
     * @param x
     * @param y
     * @throws java.lang.Exception
     */
    public Pared(int x,int y) throws Exception {
        super(x,y,true,false,false);
        Image imagen = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/pared.png"));
        this.setImagen(imagen);
    }
    
    @Override
    public boolean espacioBloqueado(ArrayList<ArrayList<Espacio>> pMatriz, int pCantRevisones
            ,Espacio pEspacioAnterior) {
        return false;
    }

   
    
}
