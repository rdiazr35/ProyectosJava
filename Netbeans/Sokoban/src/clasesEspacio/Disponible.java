/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesEspacio;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import superClases.Espacio;

/**
 *Clase para el tipo de espacio Disponible, sobre el cual 
 * se puede mover los objetos cajas y personaje
 * @author Administrador
 */
public class Disponible extends Espacio{
    
    /**
     * Constructor de la clase Disponible
     * @param x
     * @param y
     * @throws java.lang.Exception
     */
    public Disponible(int x,int y) throws Exception {
        super(x,y,false,false,false);
        //Image imagen = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/disponible.jpg"));
        this.setImagen(null);
    }

    @Override
    public boolean espacioBloqueado(ArrayList<ArrayList<Espacio>> pMatriz, int pCantRevisones
    ,Espacio pEspacioAnterior) {
        return false;
    }

    

    
    
}
