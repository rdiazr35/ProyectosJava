/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import Excepciones.ExcepcionNivel;
import clases.Nivel;
import clasesEspacio.Caja;
import clasesEspacio.CajaPunto;
import clasesEspacio.Disponible;
import clasesEspacio.Pared;
import clasesEspacio.Personaje;
import clasesEspacio.Punto;
import clasesUsuario.Administrador;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import superClases.Espacio;

/**
 *
 * @author Administrador
 */
public class MantNiveles extends javax.swing.JFrame {

    private Nivel nivelSeleccionado;
    private ArrayList<ArrayList<Espacio>> matrizModificada;
    private boolean cambiosMatrizModificada = false;

    /**
     * Creates new form MantNiveles
     */
    public MantNiveles() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.dialogMatrizNivel.setLocationRelativeTo(null);
        Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/caja.png"));
        Image imageDialog = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/editar_matriz.png"));
        this.setIconImage(image);
        this.dialogMatrizNivel.setIconImage(imageDialog);
        cargarNiveles();

    }

    private void cargarNiveles() {
        DefaultTableModel model = (DefaultTableModel) grdNiveles.getModel();
        model.setRowCount(0);
        ArrayList<Nivel> niveles = new ArrayList<>();
        niveles = sokoban.Sokoban.getNiveles();
        Object rowData[] = new Object[3];
        for (Nivel nivel : niveles) {
            rowData[0] = nivel.getNumeroNivel();
            rowData[1] = nivel.getBestMoves();
            rowData[2] = nivel.getUsuarioAdministrador().getNombreCompleto();
            model.addRow(rowData);
        }

        grdNiveles.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (grdNiveles.getSelectedRow() >= 0) {
                    //sacamos el valor del codigo de usuario
                    int lNumeroNivel = Integer.valueOf(grdNiveles.getValueAt(grdNiveles.getSelectedRow(), 0).toString());
                    nivelSeleccionado = sokoban.Sokoban.buscarNivel(lNumeroNivel);
                    if (nivelSeleccionado != null) {
                        mostrarInformacionNivelSeleccionado();
                    } else {
                        limpiarInformacionNivel();
                    }
                }
            }
        });

        if (model.getRowCount() > 0) {
            if (nivelSeleccionado != null) {
                for (int i = 0; i < grdNiveles.getRowCount(); i++) {
                    if (Integer.valueOf(grdNiveles.getValueAt(i, 0).toString()) == nivelSeleccionado.getNumeroNivel()) {
                        grdNiveles.changeSelection(i, 0, false, false);
                    }
                }
            } else {
                grdNiveles.changeSelection(0, 0, false, false);
            }
        }
    }

    private void mostrarInformacionNivelSeleccionado() {
        this.txtNumeroNivel.setValue(nivelSeleccionado.getNumeroNivel());
        this.txtBestMoves.setValue(nivelSeleccionado.getBestMoves());
        this.txtUsuarioAdministrador.setText(nivelSeleccionado.getUsuarioAdministrador().getNombreCompleto());

    }

    private void limpiarInformacionNivel() {
        this.txtNumeroNivel.setValue(null);
        this.txtBestMoves.setValue(null);
        this.txtUsuarioAdministrador.setText("");
    }

    private void mostrarMatriz() {
        try {
            pnlCentral.removeAll();
            pnlCentral.setLayout(null);
            pnlCentral.repaint();
            pnlCentral.setLayout(new GridBagLayout());
            for (ArrayList<Espacio> fila : matrizModificada) {
                int indexFila = matrizModificada.indexOf(fila);
                for (Espacio espacio : fila) {
                    int indexColumna = fila.indexOf(espacio);
                    JLabel lbl = new JLabel("");
                    if (espacio.getImagen() != null) {
                        ImageIcon icon = new ImageIcon(espacio.getImagen());
                        lbl.setIcon(icon);
                    } else {
                        lbl.setPreferredSize(new Dimension(32, 32));
                    }

                    lbl.setOpaque(true);
                    lbl.setBackground(new java.awt.Color(0, 153, 153));
                    lbl.setFocusable(true);

                    lbl.addMouseListener(new MouseListener() {
                        
                        @Override
                        public void mouseClicked(MouseEvent e) {

                            String nombre = lbl.getName();
                            String[] posiciones = nombre.split("_");
                            try {
                                Nivel.cambiarTipoEspacio(matrizModificada, Integer.valueOf(posiciones[0]), Integer.valueOf(posiciones[1]));
                                cambiosMatrizModificada = true;
                            } catch (Exception ex) {
                                Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
                            } finally {
                                mostrarMatriz();
                            }
                        }

                        @Override
                        public void mousePressed(MouseEvent e) {

                        }

                        @Override
                        public void mouseReleased(MouseEvent e) {

                        }

                        @Override
                        public void mouseEntered(MouseEvent e) {

                        }

                        @Override
                        public void mouseExited(MouseEvent e) {

                        }

                    });

                    lbl.setName(String.valueOf(indexFila) + "_" + String.valueOf(indexColumna));
                    GridBagConstraints constraints = new GridBagConstraints();
                    constraints.gridx = indexColumna;
                    constraints.gridy = indexFila;
                    constraints.gridwidth = 1;
                    constraints.gridheight = 1;
                    pnlCentral.add(lbl, constraints);
                }
            }
        } finally {
            pnlCentral.validate();
            pnlCentral.repaint();

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dialogMatrizNivel = new javax.swing.JDialog();
        menuEdicionMatriz = new javax.swing.JToolBar();
        btnModificarMatriz = new javax.swing.JButton();
        separador6 = new javax.swing.JToolBar.Separator();
        btnSalirMatriz = new javax.swing.JButton();
        pnlIzquierdo = new javax.swing.JPanel();
        btnAgregarColumnaIzq = new javax.swing.JButton();
        btnRemoverColumnaIzq = new javax.swing.JButton();
        pnlInferior = new javax.swing.JPanel();
        btnRemoverFilaBot = new javax.swing.JButton();
        btnAgregarFilaBot = new javax.swing.JButton();
        pnlDerecho = new javax.swing.JPanel();
        btnAgregarColumnaDer = new javax.swing.JButton();
        btnRemoverColumnaDer = new javax.swing.JButton();
        pnlSuperior = new javax.swing.JPanel();
        btnAgregarFilaTop = new javax.swing.JButton();
        btnRemoverFilaTop = new javax.swing.JButton();
        pnlCentral = new javax.swing.JPanel();
        menu = new javax.swing.JToolBar();
        btnNuevo = new javax.swing.JButton();
        separador1 = new javax.swing.JToolBar.Separator();
        btnEliminar = new javax.swing.JButton();
        separador2 = new javax.swing.JToolBar.Separator();
        btnModificar = new javax.swing.JButton();
        separador3 = new javax.swing.JToolBar.Separator();
        btnSalir = new javax.swing.JButton();
        pndDatosGenerales = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNumeroNivel = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        txtBestMoves = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        txtUsuarioAdministrador = new javax.swing.JTextField();
        btnEditarMatriz = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        grdNiveles = new javax.swing.JTable();

        dialogMatrizNivel.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        dialogMatrizNivel.setTitle("Matriz de Nivel");
        dialogMatrizNivel.setMinimumSize(new java.awt.Dimension(1250, 800));
        dialogMatrizNivel.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        dialogMatrizNivel.setResizable(false);
        dialogMatrizNivel.setSize(new java.awt.Dimension(1250, 800));
        dialogMatrizNivel.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                dialogMatrizNivelWindowClosing(evt);
            }
        });

        menuEdicionMatriz.setFloatable(false);
        menuEdicionMatriz.setRollover(true);

        btnModificarMatriz.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/editar_nivel.png"))); // NOI18N
        btnModificarMatriz.setToolTipText("Guardar los cambios para el nivel seleccionado");
        btnModificarMatriz.setFocusable(false);
        btnModificarMatriz.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnModificarMatriz.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnModificarMatriz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarMatrizActionPerformed(evt);
            }
        });
        menuEdicionMatriz.add(btnModificarMatriz);
        menuEdicionMatriz.add(separador6);

        btnSalirMatriz.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrar_ventana.png"))); // NOI18N
        btnSalirMatriz.setToolTipText("Salir del mantenimiento de Usuarios");
        btnSalirMatriz.setFocusable(false);
        btnSalirMatriz.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalirMatriz.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalirMatriz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirMatrizActionPerformed(evt);
            }
        });
        menuEdicionMatriz.add(btnSalirMatriz);

        btnAgregarColumnaIzq.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar_columna.png"))); // NOI18N
        btnAgregarColumnaIzq.setToolTipText("Agregar una nueva columna a la Izquierda en la matriz");
        btnAgregarColumnaIzq.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAgregarColumnaIzq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarColumnaIzqActionPerformed(evt);
            }
        });

        btnRemoverColumnaIzq.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar_columna.png"))); // NOI18N
        btnRemoverColumnaIzq.setToolTipText("Eliminar la columna de la Izquierda en la matriz");
        btnRemoverColumnaIzq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverColumnaIzqActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlIzquierdoLayout = new javax.swing.GroupLayout(pnlIzquierdo);
        pnlIzquierdo.setLayout(pnlIzquierdoLayout);
        pnlIzquierdoLayout.setHorizontalGroup(
            pnlIzquierdoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIzquierdoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlIzquierdoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRemoverColumnaIzq, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarColumnaIzq))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlIzquierdoLayout.setVerticalGroup(
            pnlIzquierdoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIzquierdoLayout.createSequentialGroup()
                .addGap(320, 320, 320)
                .addComponent(btnAgregarColumnaIzq, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRemoverColumnaIzq)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnRemoverFilaBot.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar_fila.png"))); // NOI18N
        btnRemoverFilaBot.setToolTipText("Eliminar la fila inferior de la matriz");
        btnRemoverFilaBot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverFilaBotActionPerformed(evt);
            }
        });

        btnAgregarFilaBot.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar_fila.png"))); // NOI18N
        btnAgregarFilaBot.setToolTipText("Agregar una nueva fila abajo en la columna");
        btnAgregarFilaBot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarFilaBotActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlInferiorLayout = new javax.swing.GroupLayout(pnlInferior);
        pnlInferior.setLayout(pnlInferiorLayout);
        pnlInferiorLayout.setHorizontalGroup(
            pnlInferiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInferiorLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAgregarFilaBot)
                .addGap(18, 18, 18)
                .addComponent(btnRemoverFilaBot)
                .addGap(471, 471, 471))
        );
        pnlInferiorLayout.setVerticalGroup(
            pnlInferiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnRemoverFilaBot)
            .addComponent(btnAgregarFilaBot)
        );

        btnAgregarColumnaDer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar_columna.png"))); // NOI18N
        btnAgregarColumnaDer.setToolTipText("Agregar una nueva columna a la Derecha en la matriz");
        btnAgregarColumnaDer.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAgregarColumnaDer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarColumnaDerActionPerformed(evt);
            }
        });

        btnRemoverColumnaDer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar_columna.png"))); // NOI18N
        btnRemoverColumnaDer.setToolTipText("Eliminar la columna de la Derecha en la matriz");
        btnRemoverColumnaDer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverColumnaDerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDerechoLayout = new javax.swing.GroupLayout(pnlDerecho);
        pnlDerecho.setLayout(pnlDerechoLayout);
        pnlDerechoLayout.setHorizontalGroup(
            pnlDerechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDerechoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDerechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRemoverColumnaDer)
                    .addComponent(btnAgregarColumnaDer))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlDerechoLayout.setVerticalGroup(
            pnlDerechoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDerechoLayout.createSequentialGroup()
                .addGap(321, 321, 321)
                .addComponent(btnAgregarColumnaDer, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRemoverColumnaDer)
                .addContainerGap(321, Short.MAX_VALUE))
        );

        btnAgregarFilaTop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar_fila.png"))); // NOI18N
        btnAgregarFilaTop.setToolTipText("Agregar una nueva fila arriba en la columna");
        btnAgregarFilaTop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarFilaTopActionPerformed(evt);
            }
        });

        btnRemoverFilaTop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar_fila.png"))); // NOI18N
        btnRemoverFilaTop.setToolTipText("Eliminar la fila superior de la matriz");
        btnRemoverFilaTop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverFilaTopActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlSuperiorLayout = new javax.swing.GroupLayout(pnlSuperior);
        pnlSuperior.setLayout(pnlSuperiorLayout);
        pnlSuperiorLayout.setHorizontalGroup(
            pnlSuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSuperiorLayout.createSequentialGroup()
                .addGap(472, 472, 472)
                .addComponent(btnAgregarFilaTop)
                .addGap(18, 18, 18)
                .addComponent(btnRemoverFilaTop)
                .addContainerGap(472, Short.MAX_VALUE))
        );
        pnlSuperiorLayout.setVerticalGroup(
            pnlSuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSuperiorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSuperiorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAgregarFilaTop)
                    .addComponent(btnRemoverFilaTop))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlCentral.setPreferredSize(new java.awt.Dimension(1024, 768));

        javax.swing.GroupLayout pnlCentralLayout = new javax.swing.GroupLayout(pnlCentral);
        pnlCentral.setLayout(pnlCentralLayout);
        pnlCentralLayout.setHorizontalGroup(
            pnlCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pnlCentralLayout.setVerticalGroup(
            pnlCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout dialogMatrizNivelLayout = new javax.swing.GroupLayout(dialogMatrizNivel.getContentPane());
        dialogMatrizNivel.getContentPane().setLayout(dialogMatrizNivelLayout);
        dialogMatrizNivelLayout.setHorizontalGroup(
            dialogMatrizNivelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogMatrizNivelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogMatrizNivelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(menuEdicionMatriz, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(dialogMatrizNivelLayout.createSequentialGroup()
                        .addComponent(pnlIzquierdo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(dialogMatrizNivelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlInferior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlCentral, javax.swing.GroupLayout.DEFAULT_SIZE, 1082, Short.MAX_VALUE)
                            .addComponent(pnlSuperior, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlDerecho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dialogMatrizNivelLayout.setVerticalGroup(
            dialogMatrizNivelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogMatrizNivelLayout.createSequentialGroup()
                .addComponent(menuEdicionMatriz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dialogMatrizNivelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlIzquierdo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlDerecho, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dialogMatrizNivelLayout.createSequentialGroup()
                        .addComponent(pnlSuperior, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlCentral, javax.swing.GroupLayout.DEFAULT_SIZE, 636, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlInferior, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Mantenimiento de Niveles");

        menu.setFloatable(false);
        menu.setRollover(true);

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar_nivel.png"))); // NOI18N
        btnNuevo.setToolTipText("Agregar un nuevo nivel con los datos digitados en pantalla");
        btnNuevo.setFocusable(false);
        btnNuevo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNuevo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        menu.add(btnNuevo);
        menu.add(separador1);

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/remover_nivel.png"))); // NOI18N
        btnEliminar.setToolTipText("Eliminar el nivel seleccionado");
        btnEliminar.setFocusable(false);
        btnEliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        menu.add(btnEliminar);
        menu.add(separador2);

        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/editar_nivel.png"))); // NOI18N
        btnModificar.setToolTipText("Guardar los cambios para el nivel seleccionado");
        btnModificar.setFocusable(false);
        btnModificar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnModificar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        menu.add(btnModificar);
        menu.add(separador3);

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrar_ventana.png"))); // NOI18N
        btnSalir.setToolTipText("Salir del mantenimiento de Usuarios");
        btnSalir.setFocusable(false);
        btnSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        menu.add(btnSalir);

        jLabel1.setText("Número de Nivel:");

        txtNumeroNivel.setToolTipText("Indica el número del nivel");

        jLabel2.setText("Best Moves:");

        jLabel3.setText("Usuario Administrador Creador:");

        txtUsuarioAdministrador.setEditable(false);
        txtUsuarioAdministrador.setToolTipText("Usuario Administrador creador del nivel");

        btnEditarMatriz.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/editar_matriz.png"))); // NOI18N
        btnEditarMatriz.setText("Editar Matriz");
        btnEditarMatriz.setToolTipText("Abrir el editor de la matriz de nivel");
        btnEditarMatriz.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEditarMatriz.setIconTextGap(5);
        btnEditarMatriz.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEditarMatriz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarMatrizActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pndDatosGeneralesLayout = new javax.swing.GroupLayout(pndDatosGenerales);
        pndDatosGenerales.setLayout(pndDatosGeneralesLayout);
        pndDatosGeneralesLayout.setHorizontalGroup(
            pndDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pndDatosGeneralesLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(pndDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pndDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pndDatosGeneralesLayout.createSequentialGroup()
                        .addComponent(txtNumeroNivel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBestMoves, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtUsuarioAdministrador, javax.swing.GroupLayout.PREFERRED_SIZE, 397, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addComponent(btnEditarMatriz)
                .addGap(34, 34, 34))
        );
        pndDatosGeneralesLayout.setVerticalGroup(
            pndDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pndDatosGeneralesLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(pndDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNumeroNivel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtBestMoves, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pndDatosGeneralesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUsuarioAdministrador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(pndDatosGeneralesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnEditarMatriz, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        grdNiveles.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Número", "Best Moves", "Usuario Administrador Creador"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        grdNiveles.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(grdNiveles);
        if (grdNiveles.getColumnModel().getColumnCount() > 0) {
            grdNiveles.getColumnModel().getColumn(0).setMinWidth(100);
            grdNiveles.getColumnModel().getColumn(0).setPreferredWidth(1);
            grdNiveles.getColumnModel().getColumn(0).setMaxWidth(1);
            grdNiveles.getColumnModel().getColumn(1).setMinWidth(100);
            grdNiveles.getColumnModel().getColumn(1).setPreferredWidth(1);
            grdNiveles.getColumnModel().getColumn(1).setMaxWidth(1);
            grdNiveles.getColumnModel().getColumn(2).setPreferredWidth(80);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(menu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pndDatosGenerales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(menu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pndDatosGenerales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed

        if (nivelSeleccionado != null) {

            int dialogResult = JOptionPane.showConfirmDialog(this,
                    "¿Esta seguro que desea eliminar definitivamente el nivel "
                    + String.valueOf(nivelSeleccionado.getNumeroNivel()) + "?", "Verificación", JOptionPane.YES_NO_OPTION);
            if (dialogResult == JOptionPane.YES_OPTION) {
                try {
                    sokoban.Sokoban.eliminarNivel(nivelSeleccionado);
                } catch (ExcepcionNivel ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(),
                            "Error Eliminando Nivel", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
                }
                nivelSeleccionado = null;
                cargarNiveles();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Debe de seleccionar el usuario que desea eliminar",
                    "Usuario NO seleccionado", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        if (nivelSeleccionado != null) {
            try {

                txtNumeroNivel.commitEdit();
                txtBestMoves.commitEdit();
                if (Integer.valueOf(txtNumeroNivel.getValue().toString()) > sokoban.Sokoban.getNiveles().size()) {
                    throw new ExcepcionNivel("El número del nivel no puede superar la cantidad de niveles actuales("
                            + sokoban.Sokoban.getNiveles().size() + ")");
                }

                boolean reordenar = false;
                if (txtNumeroNivel.getValue().toString() != String.valueOf(nivelSeleccionado.getNumeroNivel())) {
                    reordenar = true;
                }

                nivelSeleccionado.setNumeroNivel(Integer.valueOf(txtNumeroNivel.getValue().toString()));
                nivelSeleccionado.setBestMoves(Integer.valueOf(txtBestMoves.getValue().toString()));

                if (reordenar) {
                    sokoban.Sokoban.cambiarOrdenNivel(nivelSeleccionado);
                }

            } catch (ExcepcionNivel ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        "Error Modificando Nivel", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        "Error obteniendo el número de nivel o el best move", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                cargarNiveles();
            }
        }
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        try {
            txtNumeroNivel.commitEdit();
            txtBestMoves.commitEdit();
            Nivel nivel = sokoban.Sokoban.crearNivel(Integer.valueOf(txtNumeroNivel.getValue().toString()),
                    Integer.valueOf(txtBestMoves.getValue().toString()), (Administrador) sokoban.Sokoban.getUsuarioActual());
            nivelSeleccionado = nivel;
            sokoban.Sokoban.cambiarOrdenNivel(nivelSeleccionado);
        } catch (ExcepcionNivel ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error Agregando Nivel", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error obteniendo el número de nivel o el best move", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cargarNiveles();
        }
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void copiarMatrizOriginal() throws Exception {
        this.matrizModificada = new ArrayList<ArrayList<Espacio>>();
        //copiarmos la original para modificarla sin alterar la original
        for (ArrayList<Espacio> fila : nivelSeleccionado.getEstadoInicial()) {
            ArrayList<Espacio> filaCopia = new ArrayList<>();
            for (Espacio columna : fila) {
                Espacio columnaCopia;
                if (columna instanceof Caja) {
                    columnaCopia = new Caja(nivelSeleccionado.getEstadoInicial().indexOf(fila), fila.indexOf(columna));
                } else if (columna instanceof CajaPunto) {
                    columnaCopia = new CajaPunto(nivelSeleccionado.getEstadoInicial().indexOf(fila), fila.indexOf(columna));
                } else if (columna instanceof Disponible) {
                    columnaCopia = new Disponible(nivelSeleccionado.getEstadoInicial().indexOf(fila), fila.indexOf(columna));
                } else if (columna instanceof Pared) {
                    columnaCopia = new Pared(nivelSeleccionado.getEstadoInicial().indexOf(fila), fila.indexOf(columna));
                } else if (columna instanceof Personaje) {
                    columnaCopia = new Personaje(nivelSeleccionado.getEstadoInicial().indexOf(fila), fila.indexOf(columna));
                } else if (columna instanceof Punto) {
                    columnaCopia = new Punto(nivelSeleccionado.getEstadoInicial().indexOf(fila), fila.indexOf(columna));
                }
                filaCopia.add(columna);
            }
            this.matrizModificada.add(filaCopia);
        }
    }

    private void btnEditarMatrizActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarMatrizActionPerformed
        if (nivelSeleccionado != null) {
            try {
                this.cambiosMatrizModificada = false;
                copiarMatrizOriginal();
                mostrarMatriz();
                dialogMatrizNivel.setVisible(true);

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        "Error abriendo el editor de la matriz del nivel", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }//GEN-LAST:event_btnEditarMatrizActionPerformed

    private void btnModificarMatrizActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarMatrizActionPerformed
        try {
            this.nivelSeleccionado.setEstadoInicial(matrizModificada);
            this.cambiosMatrizModificada = false;
            JOptionPane.showMessageDialog(this, "La matriz se ha guardado correctamente",
                    "Guardado", JOptionPane.INFORMATION_MESSAGE);
        } catch (ExcepcionNivel ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error guardando matriz del nivel", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_btnModificarMatrizActionPerformed

    private boolean verificarSalirDialogoMatriz() {
        int dialogResult = 0;
        if (cambiosMatrizModificada) {
            dialogResult = JOptionPane.showConfirmDialog(this,
                    "La matriz cuenta con cambios pendientes por aplicar \n ¿Desea salir sin aplicar estos cambios?",
                    "Verificación", JOptionPane.YES_NO_OPTION);
        }
        if (dialogResult == JOptionPane.YES_OPTION) {
            return true;
        } else {
            return false;
        }
    }

    private void btnSalirMatrizActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirMatrizActionPerformed
        if (verificarSalirDialogoMatriz()) {
            this.dialogMatrizNivel.setVisible(false);
        }

    }//GEN-LAST:event_btnSalirMatrizActionPerformed

    private void btnRemoverColumnaIzqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverColumnaIzqActionPerformed
        removerColumnaMatrizModificada('I');
        actualizarDireccionesEspacios();
    }//GEN-LAST:event_btnRemoverColumnaIzqActionPerformed

    private void btnRemoverColumnaDerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverColumnaDerActionPerformed
        removerColumnaMatrizModificada('D');
        actualizarDireccionesEspacios();
    }//GEN-LAST:event_btnRemoverColumnaDerActionPerformed

    private void btnAgregarColumnaIzqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarColumnaIzqActionPerformed
        try {
            agregarColumnaMatrizModificada('I');
            actualizarDireccionesEspacios();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error agregando columna izquierda", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAgregarColumnaIzqActionPerformed

    private void btnAgregarColumnaDerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarColumnaDerActionPerformed
        try {
            agregarColumnaMatrizModificada('D');
            actualizarDireccionesEspacios();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error agregando columna derecha", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAgregarColumnaDerActionPerformed

    private void btnAgregarFilaTopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarFilaTopActionPerformed
        try {
            agregarFilaMatrizModificada('T');
            actualizarDireccionesEspacios();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error agregando fila al inicio de la matriz", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAgregarFilaTopActionPerformed

    private void btnAgregarFilaBotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarFilaBotActionPerformed
        try {
            agregarFilaMatrizModificada('B');
            actualizarDireccionesEspacios();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Error agregando fila al final de la matriz", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MantNiveles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAgregarFilaBotActionPerformed

    private void btnRemoverFilaTopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverFilaTopActionPerformed
        removerFilaMatrizModificada('T');
        actualizarDireccionesEspacios();
    }//GEN-LAST:event_btnRemoverFilaTopActionPerformed

    private void btnRemoverFilaBotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverFilaBotActionPerformed
        removerFilaMatrizModificada('B');
        actualizarDireccionesEspacios();
    }//GEN-LAST:event_btnRemoverFilaBotActionPerformed

    private void dialogMatrizNivelWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_dialogMatrizNivelWindowClosing
        if (verificarSalirDialogoMatriz()) {
            this.dialogMatrizNivel.setVisible(false);
        }
    }//GEN-LAST:event_dialogMatrizNivelWindowClosing

    private void agregarFilaMatrizModificada(char pUbicacion) throws Exception {
        try {
            if (matrizModificada.size() < 19) {

                ArrayList<Espacio> nuevaFila = new ArrayList<>();
                //si ya tengo filas entonces se ingresan la cantidad de columnas que contiene la primera existente
                if (matrizModificada.size() > 0) {
                    for (Espacio espacio : matrizModificada.get(0)) {
                        Espacio nuevaColumna;
                        if (pUbicacion == 'T') {
                            nuevaColumna = new Disponible(0, matrizModificada.indexOf(espacio));
                        } else {
                            nuevaColumna = new Disponible(matrizModificada.size(), matrizModificada.indexOf(espacio));
                        }

                        nuevaFila.add(nuevaColumna);
                    }
                } else {
                    Espacio nuevaColumna = new Disponible(0, 0);
                    nuevaFila.add(nuevaColumna);
                }

                if (pUbicacion == 'T') {
                    matrizModificada.add(0, nuevaFila);
                } else {
                    matrizModificada.add(nuevaFila);
                }
                cambiosMatrizModificada = true;
            } else {
                JOptionPane.showMessageDialog(this, "La matriz NO puede tener más de 19 Filas!!!",
                        "Cantidad de Filas máximas", JOptionPane.INFORMATION_MESSAGE);
            }

        } finally {
            this.mostrarMatriz();
        }

    }

    private void removerFilaMatrizModificada(char pUbicacion) {
        try {
            if (matrizModificada.size() > 3) {
                if (pUbicacion == 'T') {
                    matrizModificada.remove(0);
                } else {
                    matrizModificada.remove(matrizModificada.size() - 1);
                }
                cambiosMatrizModificada = true;
            } else {
                JOptionPane.showMessageDialog(this, "La matriz NO puede tener menos de 3 Filas!!!",
                        "Cantidad de Filas mínimas", JOptionPane.INFORMATION_MESSAGE);
            }
        } finally {
            mostrarMatriz();
        }
    }

    private void agregarColumnaMatrizModificada(char pUbicacion) throws Exception {
        try {
            if (matrizModificada.get(0).size() < 33) {
                for (ArrayList<Espacio> fila : matrizModificada) {
                    Espacio nuevaColumna;
                    if (pUbicacion == 'I') {
                        nuevaColumna = new Disponible(matrizModificada.indexOf(fila), 0);
                        fila.add(0, nuevaColumna);
                    } else {
                        nuevaColumna = new Disponible(matrizModificada.indexOf(fila), fila.size());
                        fila.add(nuevaColumna);
                    }
                }
                cambiosMatrizModificada = true;
            } else {
                JOptionPane.showMessageDialog(this, "La matriz NO puede tener más de 33 Columnas!!!",
                        "Cantidad de Columnas máximas", JOptionPane.INFORMATION_MESSAGE);
            }

        } finally {
            mostrarMatriz();
        }
    }

    private void removerColumnaMatrizModificada(char pUbicacion) {
        try {
            if (matrizModificada.get(0).size() > 3) {
                for (ArrayList<Espacio> fila : matrizModificada) {
                    if (pUbicacion == 'I') {
                        fila.remove(0);
                    } else {
                        fila.remove(fila.size() - 1);
                    }
                }
                cambiosMatrizModificada = true;
            } else {
                JOptionPane.showMessageDialog(this, "La matriz NO puede tener menos de 3 Columnas!!!",
                        "Cantidad de Columnas mínimas", JOptionPane.INFORMATION_MESSAGE);
            }
        } finally {
            mostrarMatriz();
        }
    }

    private void actualizarDireccionesEspacios() {
        for (int f = 0; f < matrizModificada.size(); f++) {
            ArrayList<Espacio> fila = matrizModificada.get(f);
            for (int c = 0; c < fila.size(); c++) {
                Espacio espacio = fila.get(c);
                espacio.setX(f);
                espacio.setY(c);
            }

        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarColumnaDer;
    private javax.swing.JButton btnAgregarColumnaIzq;
    private javax.swing.JButton btnAgregarFilaBot;
    private javax.swing.JButton btnAgregarFilaTop;
    private javax.swing.JButton btnEditarMatriz;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnModificarMatriz;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnRemoverColumnaDer;
    private javax.swing.JButton btnRemoverColumnaIzq;
    private javax.swing.JButton btnRemoverFilaBot;
    private javax.swing.JButton btnRemoverFilaTop;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnSalirMatriz;
    private javax.swing.JDialog dialogMatrizNivel;
    private javax.swing.JTable grdNiveles;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar menu;
    private javax.swing.JToolBar menuEdicionMatriz;
    private javax.swing.JPanel pndDatosGenerales;
    private javax.swing.JPanel pnlCentral;
    private javax.swing.JPanel pnlDerecho;
    private javax.swing.JPanel pnlInferior;
    private javax.swing.JPanel pnlIzquierdo;
    private javax.swing.JPanel pnlSuperior;
    private javax.swing.JToolBar.Separator separador1;
    private javax.swing.JToolBar.Separator separador2;
    private javax.swing.JToolBar.Separator separador3;
    private javax.swing.JToolBar.Separator separador6;
    private javax.swing.JSpinner txtBestMoves;
    private javax.swing.JSpinner txtNumeroNivel;
    private javax.swing.JTextField txtUsuarioAdministrador;
    // End of variables declaration//GEN-END:variables
}
