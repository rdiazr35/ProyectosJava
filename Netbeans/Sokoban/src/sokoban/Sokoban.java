/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sokoban;

import Excepciones.ExcepcionGrupo;
import Excepciones.ExcepcionNivel;
import Excepciones.ExcepcionUsuario;
import clases.Configuracion;
import clases.Estadistica;
import clases.Grupo;
import clases.Juego;
import clases.JuegoNivel;
import clases.Nivel;
import clasesEspacio.Caja;
import clasesEspacio.Disponible;
import clasesEspacio.Pared;
import clasesEspacio.Personaje;
import clasesEspacio.Punto;
import superClases.Usuario;
import interfaz.Principal;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.UIManager;
import clasesUsuario.Administrador;
import clasesUsuario.Jugador;
import java.util.Collections;
import java.util.Comparator;
import superClases.Espacio;

/**
 *
 * @author Administrador
 */
public class Sokoban {

    private static ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
    private static Usuario usuarioActual;
    private static ArrayList<Nivel> niveles = new ArrayList<Nivel>();
    private static ArrayList<Grupo> grupos = new ArrayList<Grupo>();
    private static Grupo grupoActual = null;
    private static Configuracion configuracion = new Configuracion(1, true);
    private static Principal pantallaPricipal;
    private static String matrizNivelesIniciales[] = {
        "M    #####          "
        + "M    #   #          "
        + "M    #   #          "
        + "M  ###   ##         "
        + "M  #  $   #         "
        + "M### # ## #   ######"
        + "M#   # ## #####   .#"
        + "M# $               #"
        + "M##### ### #@##   .#"
        + "M    #     #########"
        + "M    #######        ",
          "M############  "
        + "M#.   #     ###"
        + "M#    # $     #"
        + "M#.   #$####  #"
        + "M#      @ ##  #"
        + "M#.   # #    ##"
        + "M###### ##    #"
        + "M  # $        #"
        + "M  #    #     #"
        + "M  ############",
          "M        ######## "
        + "M        #     @# "
        + "M        # $#$ ## "
        + "M        #     #  "
        + "M        ##    #  "
        + "M#########   # ###"
        + "M#.     ##       #"
        + "M##.      $      #"
        + "M#.     ##########"
        + "M########         ",
          "M           ########"
        + "M           #     .#"
        + "M############      #"
        + "M#    #  $ $      .#"
        + "M#    #     #      #"
        + "M#          #     .#"
        + "M#    #     ########"
        + "M#  $ #     #       "
        + "M## #########       "
        + "M#    #    ##       "
        + "M#         ##       "
        + "M#    #    @#       "
        + "M#    #    ##       "
        + "M###########        ",
          "M        #####    "
        + "M        #   #####"
        + "M        # #$##  #"
        + "M        #       #"
        + "M######### ###   #"
        + "M#.     ##     ###"
        + "M#.       $    ## "
        + "M#.     ##     @# "
        + "M#########     ## "
        + "M        # $    # "
        + "M        ### ## # "
        + "M          #    # "
        + "M          ###### "};

    /**
     *
     * @return
     */
    public static ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    /**
     *
     * @param usuarios
     */
    public static void setUsuarios(ArrayList<Usuario> usuarios) {
        Sokoban.usuarios = usuarios;
    }

    public static void setUsuarioActual(Usuario usuarioActual) {
        Sokoban.usuarioActual = usuarioActual;
    }

    /**
     *
     * @return
     */
    public static ArrayList<Nivel> getNiveles() {
        return niveles;
    }

    /**
     *
     * @param niveles
     */
    public static void setNiveles(ArrayList<Nivel> niveles) {
        Sokoban.niveles = niveles;
    }

    /**
     *
     * @return
     */
    public static ArrayList<Grupo> getGrupos() {
        return grupos;
    }

    /**
     *
     * @param grupos
     */
    public static void setGrupos(ArrayList<Grupo> grupos) {
        Sokoban.grupos = grupos;
    }

    /**
     *
     * @return
     */
    public static Configuracion getConfiguracion() {
        return configuracion;
    }

    /**
     *
     * @param configuracion
     */
    public static void setConfiguracion(Configuracion configuracion) {
        Sokoban.configuracion = configuracion;
    }

    /**
     *
     * @return
     */
    public static Principal getPantallaPricipal() {
        return pantallaPricipal;
    }

    /**
     *
     * @param pantallaPricipal
     */
    public static void setPantallaPricipal(Principal pantallaPricipal) {
        Sokoban.pantallaPricipal = pantallaPricipal;
    }

    /**
     *
     * @return
     */
    public static Usuario getUsuarioActual() {
        return usuarioActual;
    }

    /**
     *
     * @return
     */
    public static Grupo getGrupoActual() {
        return grupoActual;
    }

    /**
     *
     * @param grupoActual
     */
    public static void setGrupoActual(Grupo grupoActual) {
        Sokoban.grupoActual = grupoActual;
    }

    /**
     * @param args the command line arguments
     * @throws Excepciones.ExcepcionUsuario
     * @throws Excepciones.ExcepcionNivel
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws ExcepcionUsuario, ExcepcionNivel, Exception {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        crearDatosIniciales();
        Principal.main(args);

        //Login.main(args);
    }

    private static void crearDatosIniciales() throws ExcepcionUsuario, ExcepcionNivel, Exception {
        //Image imagenAdministrador = Toolkit.getDefaultToolkit().getImage("/imagenes/fotoAdministrador.png");

        Image imagen = Toolkit.getDefaultToolkit().getImage(Sokoban.class.getResource("/imagenes/fotoAdministrador.png"));
        Image imagenJugador = Toolkit.getDefaultToolkit().getImage(Sokoban.class.getResource("/imagenes/fotoJugador.png"));
        Image imagenDefecto = Toolkit.getDefaultToolkit().getImage(Sokoban.class.getResource("/imagenes/fotoDefecto.png"));

        Administrador usuAd = (Administrador) crearUsuarioAdministrador("admin", "123", "Administrador 1 Sokoban", "102340567", "admin1@sokoban.com", imagen, "Costa Rica");
        crearUsuarioAdministrador("admin2", "123", "Administrador 2 Sokoban", "102340567", "admin2@sokoban.com", imagenDefecto, "Nicaragua");
        Administrador usuAd3 = (Administrador) crearUsuarioAdministrador("admin3", "123", "Administrador 3 Sokoban", "102340567", "admin3@sokoban.com", imagen, "Panamá");
        crearUsuarioAdministrador("admin4", "123", "Administrador 4 Sokoban", "102340567", "admin4@sokoban.com", imagenDefecto, "Otro");

        crearUsuarioJugador("derek", "Derek Somarriba", "209500200", "derek@cgsapps.com", "123", imagenJugador);
        crearUsuarioJugador("ivan", "Ivan Rojas", "102340567", "ivan@cgsapps.com", "123", imagenJugador);

        Nivel nivel = crearNivel(1, 300, usuAd);
        generarMatrizNivelInicial(nivel);
        nivel = crearNivel(2, 300, usuAd3);
        generarMatrizNivelInicial(nivel);
        nivel = crearNivel(3, 300, usuAd);
        generarMatrizNivelInicial(nivel);
        nivel = crearNivel(4, 300, usuAd3);
        generarMatrizNivelInicial(nivel);
        nivel = crearNivel(5, 300, usuAd3);
        generarMatrizNivelInicial(nivel);
    }

    private static void generarMatrizNivelInicial(Nivel nivel) throws Exception {
        char[] matriz = matrizNivelesIniciales[(nivel.getNumeroNivel() - 1)].toCharArray();
        ArrayList<ArrayList<Espacio>> estadoInicial = new ArrayList<>();
        ArrayList<Espacio> fila = null;
        Espacio espacio = null;
        int contFila = -1;
        int contColumna = 0;

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i] == 'M') {
                fila = new ArrayList<Espacio>();
                estadoInicial.add(fila);
                contFila++;
                contColumna = 0;
            } else {
                switch (matriz[i]) {
                    case ' ':
                        espacio = new Disponible(contFila, contColumna);
                        break;
                    case '#':
                        espacio = new Pared(contFila, contColumna);
                        break;
                    case '.':
                        espacio = new Punto(contFila, contColumna);
                        break;
                    case '@':
                        espacio = new Personaje(contFila, contColumna);
                        break;
                    case '$':
                        espacio = new Caja(contFila, contColumna);
                        break;
                    default:
                        break;
                }
                fila.add(espacio);
                contColumna++;
            }

        }
        nivel.setEstadoInicial(estadoInicial);

    }

    /**
     *
     * @param pCodUsuario
     * @param pPassword
     * @return
     */
    public static boolean validarUsuario(String pCodUsuario, String pPassword) {
        for (Usuario usuario : usuarios) {
            if ((usuario.getCodUsuario().equals(pCodUsuario)) && (usuario.getPassword().equals(pPassword))) {
                usuarioActual = usuario;
                return true;
            }

        }
        return false;
    }

    /**
     *
     * @return
     */
    public static ArrayList<Usuario> obtenerUsuariosAdministradores() {
        ArrayList<Usuario> lUsuariosAdministradores = new ArrayList<Usuario>();
        for (Usuario usuario : usuarios) {
            if (usuario instanceof Administrador) {
                lUsuariosAdministradores.add(usuario);

            }

        }
        return lUsuariosAdministradores;
    }

    /**
     *
     * @return
     */
    public static ArrayList<Usuario> obtenerUsuariosJugadores() {
        ArrayList<Usuario> lUsuariosJugadores = new ArrayList<Usuario>();
        for (Usuario usuario : usuarios) {
            if (usuario instanceof Jugador) {
                lUsuariosJugadores.add(usuario);

            }

        }
        return lUsuariosJugadores;
    }

    /**
     *
     * @param pCodUsuario
     * @return
     */
    public static Usuario buscarUsuario(String pCodUsuario) {
        for (Usuario usuario : usuarios) {
            if (usuario.getCodUsuario().equals(pCodUsuario)) {
                return usuario;
            }
        }
        return null;
    }

    /**
     *
     * @param pNumeroNivel
     * @return
     */
    public static Nivel buscarNivel(int pNumeroNivel) {
        for (Nivel nivel : niveles) {
            if (nivel.getNumeroNivel() == pNumeroNivel) {
                return nivel;
            }
        }
        return null;
    }

    /**
     *
     * @param pJuegoNivel
     * @return
     */
    public static JuegoNivel buscarJuegoNivel(JuegoNivel pJuegoNivel) {

        for (Usuario jugadores : obtenerUsuariosJugadores()) {
            for (Juego juegos : ((Jugador) jugadores).getJuegos()) {
                for (JuegoNivel jn : juegos.getNivelesJugador()) {
                    if (jn.equals(pJuegoNivel)) {
                        return jn;
                    }
                }
            }

        }
        return null;
    }

    /**
     *
     * @param pCodUsuario
     * @param pPassword
     * @param pNombre
     * @param pCedula
     * @param pEmail
     * @param pFotografia
     * @param pNacionalidad
     * @return
     * @throws Excepciones.ExcepcionUsuario
     */
    public static Usuario crearUsuarioAdministrador(String pCodUsuario, String pPassword,
            String pNombre, String pCedula, String pEmail, Image pFotografia, String pNacionalidad) throws ExcepcionUsuario {

        Administrador usuarioAdmin = new Administrador(pNacionalidad, pCodUsuario, pNombre, pCedula, pEmail,
                pPassword, pFotografia);

        usuarios.add(usuarioAdmin);
        return usuarioAdmin;
    }

    /**
     *
     * @param pCodUsuario
     * @param pNombre
     * @param pCedula
     * @param pEmail
     * @param pPassword
     * @param pFotoGrafia
     * @return
     * @throws Excepciones.ExcepcionUsuario
     */
    public static Usuario crearUsuarioJugador(String pCodUsuario,
            String pNombre, String pCedula, String pEmail, String pPassword, Image pFotoGrafia) throws ExcepcionUsuario {
        Jugador jugador = new Jugador(pCodUsuario, pNombre, pCedula, pEmail, pPassword, pFotoGrafia);
        usuarios.add(jugador);
        return jugador;
    }

    /**
     *
     * @param pCodUsuario
     */
    public static void eliminarUsuario(String pCodUsuario) {
        Usuario usuario = buscarUsuario(pCodUsuario);
        if (usuario == usuarioActual) {
            usuarioActual = null;
        }

        usuarios.remove(usuario);

    }

    /**
     *
     * @param pNivel
     * @throws Excepciones.ExcepcionNivel
     */
    public static void cambiarOrdenNivel(Nivel pNivel) throws ExcepcionNivel {
        if (pNivel.getNumeroNivel() <= niveles.size()) {

            int indiceNivelPreviamente = niveles.indexOf(pNivel);
            int indiceNivelAnterior = pNivel.getNumeroNivel() - 1;

            if (indiceNivelAnterior != indiceNivelPreviamente) {

                Nivel nivelAnterior = niveles.get(pNivel.getNumeroNivel() - 1);

                niveles.set(pNivel.getNumeroNivel() - 1, pNivel);
                niveles.remove(indiceNivelPreviamente);
                if (indiceNivelAnterior < indiceNivelPreviamente) {
                    niveles.add(nivelAnterior.getNumeroNivel(), nivelAnterior);
                } else {
                    niveles.add(nivelAnterior.getNumeroNivel() - 2, nivelAnterior);
                }
                cambiarOrdenNivel();
            }
        }
    }

    private static void cambiarOrdenNivel() throws ExcepcionNivel {
        for (Nivel nivel : niveles) {
            nivel.setNumeroNivel(niveles.indexOf(nivel) + 1);
        }

    }

    /**
     *
     * @param pNumeroNivel
     * @param pBestMoves
     * @param pUsuario
     * @return
     * @throws Excepciones.ExcepcionNivel
     * @throws java.lang.Exception
     */
    public static Nivel crearNivel(int pNumeroNivel, int pBestMoves, Administrador pUsuario) throws ExcepcionNivel, Exception {
        Nivel nivel = new Nivel(pNumeroNivel, pBestMoves, pUsuario);
        niveles.add(nivel);
        return nivel;
    }

    /**
     *
     * @param pNivel
     * @throws Excepciones.ExcepcionNivel
     */
    public static void eliminarNivel(Nivel pNivel) throws ExcepcionNivel {
        niveles.remove(pNivel);
        cambiarOrdenNivel();
    }

    /**
     *
     * @param pJugadores
     * @param pNiveles
     * @throws Excepciones.ExcepcionGrupo
     */
    public static void crearGrupo(ArrayList<Jugador> pJugadores, ArrayList<Nivel> pNiveles) throws ExcepcionGrupo, Exception {
        Grupo grupo = new Grupo(pJugadores, pNiveles);
        grupos.add(grupo);
        grupoActual = grupo;
        pasarNivelGrupo();
    }
    
    public static void pasarNivelGrupo() throws Exception
    {
       boolean lJuegoGrupo = sokoban.Sokoban.getGrupoActual().jugarGrupo();
       if(!lJuegoGrupo){
           pantallaPricipal.mostarResultadoGrupo();
         grupoActual = null;
       }
    }

    /**
     *
     * @param pCantSegundos
     * @return
     */
    public static String formatearSegundos(int pCantSegundos) {
        int cantSegundos = pCantSegundos;

        int cantHoras = cantSegundos / 3600;
        int cantMinutos = ((cantSegundos - (cantHoras * 3600)) / 60);

        cantSegundos = cantSegundos - ((cantHoras * 3600) + (cantMinutos * 60));

        String tiempo = ((cantHoras <= 9) ? "0" + cantHoras : cantHoras + "") + ":"
                + ((cantMinutos <= 9) ? "0" + cantMinutos : cantMinutos + "") + ":"
                + ((cantSegundos <= 9) ? "0" + cantSegundos : cantSegundos + "");
        return tiempo;
    }

    /**
     *
     * @param pCodAdministrador
     * @return
     */
    public static ArrayList<Nivel> imprimirInformacionNiveles(String pCodAdministrador) {
        ArrayList<Nivel> nivelesAdministrador = new ArrayList<>();
        getNiveles().stream().filter((nivel) -> (nivel.getUsuarioAdministrador().getCodUsuario().equals(pCodAdministrador))).forEach((nivel) -> {
            nivelesAdministrador.add(nivel);
        });
        return nivelesAdministrador;

    }

    /**
     *
     * @param pCodUsuario
     * @return
     */
    public static ArrayList<JuegoNivel> imprimirHistorialJugador(String pCodUsuario) {
        ArrayList<JuegoNivel> historial = new ArrayList<>();
        for (Juego juego : ((Jugador) buscarUsuario(pCodUsuario)).getJuegos()) {
            for (JuegoNivel juegoNivel : juego.getNivelesJugador()) {
                historial.add(juegoNivel);
            }

        }
        return historial;
    }

    /**
     *
     * @return
     */
    public static ArrayList<ArrayList<Object>> imprimirTopCincoJugadoresGanesOptimos() {
        ArrayList<ArrayList<Object>> topGanadoresOptimos = new ArrayList<>();

        for (Usuario jugador : obtenerUsuariosJugadores()) {
            int cantGanesOptimos = 0;
            int cantGanesOptimosUltimo = 0;
            int indexAdd = -1;

            for (Juego juego : ((Jugador) jugador).getJuegos()) {
                for (JuegoNivel jn : juego.getNivelesJugador()) {
                    if (jn.getEstado().equals("Ganado Optimizado")) {
                        cantGanesOptimos++;
                    }
                }
            }

            if (cantGanesOptimos > 0) {
                ArrayList<Object> jugadorGaneOptimo = new ArrayList<>();
                jugadorGaneOptimo.add(jugador);
                jugadorGaneOptimo.add(cantGanesOptimos);
                topGanadoresOptimos.add(jugadorGaneOptimo);
            }
        }

        Collections.sort(topGanadoresOptimos, new Comparator<ArrayList<Object>>() {
            @Override
            public int compare(ArrayList<Object> o1, ArrayList<Object> o2) {
                if ((Integer.parseInt(o1.get(1).toString())) < (Integer.parseInt(o2.get(1).toString()))) {
                    return 1;
                } else {
                    return -1;
                }
            }

        });

        while (topGanadoresOptimos.size() > 5) {
            topGanadoresOptimos.remove(topGanadoresOptimos.size() - 1);
        }

        return topGanadoresOptimos;
    }

    /**
     *
     * @return
     */
    public static ArrayList<Administrador> imprimirTopCincoAdministradoresRecordRotos() {
        ArrayList<Administrador> topRecordsRotos = new ArrayList<>();

        for (Usuario administrador : obtenerUsuariosAdministradores()) {
            if (((Administrador) administrador).getCantBestMovesSuperados() > 0) {
                topRecordsRotos.add(((Administrador) administrador));
            }
        }

        Collections.sort(topRecordsRotos, (Administrador o1, Administrador o2) -> {            
            if (o1.getCantBestMovesSuperados() < o2.getCantBestMovesSuperados()) {
                return 1;
            } else {
                return -1;
            }
        });

        while (topRecordsRotos.size() > 5) {
            topRecordsRotos.remove(topRecordsRotos.size() - 1);
        }

        return topRecordsRotos;

    }

    /**
     *
     * @param pNumeroNivel
     * @return
     */
    public static ArrayList<ArrayList<Object>> imprimirTopCincoInsistentesNivel(int pNumeroNivel) {
        ArrayList<ArrayList<Object>> topInsistentes = new ArrayList<>();

        for (Usuario jugador : obtenerUsuariosJugadores()) {
            int cantVecesJugadas = 0;
            int cantVecesUltimo = 0;
            int indexAdd = -1;

            for (Juego juego : ((Jugador) jugador).getJuegos()) {
                for (JuegoNivel jn : juego.getNivelesJugador()) {
                    if (jn.getNivel().getNumeroNivel() == pNumeroNivel) {
                        cantVecesJugadas++;
                    }
                }

            }

            //buscamos si esta dentro del top 5 para agregarlo en alguna posición específica
            for (ArrayList<Object> topInsistente : topInsistentes) {
                for (Object object : topInsistente) {
                    if (object instanceof Integer) {
                        cantVecesUltimo = Integer.valueOf(object.toString());
                        if (cantVecesJugadas > cantVecesUltimo) {
                            indexAdd = topInsistentes.indexOf(topInsistente);
                            break;
                        }
                    }
                }
                if (topInsistentes.indexOf(topInsistente) == indexAdd) {
                    break;
                }
            }

            if (cantVecesJugadas > 0) {
                if ((cantVecesJugadas <= cantVecesUltimo) && (indexAdd == -1)) {
                    indexAdd = topInsistentes.size();
                } else if (indexAdd == -1) {
                    indexAdd = 0;
                }

                ArrayList<Object> jugadorInsistente = new ArrayList<>();
                jugadorInsistente.add(jugador);
                jugadorInsistente.add(cantVecesJugadas);
                topInsistentes.add(indexAdd, jugadorInsistente);
            }

        }

        while (topInsistentes.size() > 5) {
            topInsistentes.remove(topInsistentes.size() - 1);
        }

        return topInsistentes;
    }

    /**
     *
     * @param pNumeroNivel
     * @return
     */
    public static ArrayList<ArrayList<Object>> imprimirUsuarioGananNivel(int pNumeroNivel) {
        ArrayList<ArrayList<Object>> jugadoresGanadores = new ArrayList<>();

        for (Usuario jugador : obtenerUsuariosJugadores()) {
            boolean lJugadorAgregado = false;
            ArrayList<Object> jugadorGana = new ArrayList<>();
            for (Juego juego : ((Jugador) jugador).getJuegos()) {
                for (JuegoNivel jn : juego.getNivelesJugador()) {
                    if ((jn.getNivel().getNumeroNivel() == pNumeroNivel)
                            && (jn.getEstado().contains("Ganado"))) {
                        if (!lJugadorAgregado) {
                            jugadorGana.add(jugador);
                            jugadorGana.add(jn.getCantMovimientos());
                            jugadorGana.add(jn.getCantSegundos());
                            lJugadorAgregado = true;
                        } else {
                            if (jn.getCantMovimientos() < Integer.parseInt(jugadorGana.get(1).toString())) {
                                jugadorGana.set(1, jn.getCantMovimientos());
                            }
                            if (jn.getCantSegundos() < Integer.parseInt(jugadorGana.get(2).toString())) {
                                jugadorGana.set(2, jn.getCantSegundos());
                            }
                        }
                    }
                }

            }
            if (lJugadorAgregado) {
                jugadoresGanadores.add(jugadorGana);
            }
        }

        return jugadoresGanadores;
    }

    /**
     *
     * @param pCodJugador
     * @return
     */
    public static Estadistica imprimirEstadisticasUsuario(String pCodJugador) {
        Jugador jugador = (Jugador)sokoban.Sokoban.buscarUsuario(pCodJugador);
        if(jugador != null){
           jugador.generarEstadisticas();
           return jugador.getEstadisticas();
        }
        return null;
    }

    private static void cambiarNivelesOptimizados(JuegoNivel pJuegoNivel) {
        for (Usuario usuario : obtenerUsuariosJugadores()) {
            for (Juego juego : ((Jugador) usuario).getJuegos()) {
                for (JuegoNivel jn : juego.getNivelesJugador()) {
                    if (!(jn.equals(pJuegoNivel))
                            && (jn.getNivel() == pJuegoNivel.getNivel())
                            && (jn.getEstado().equals("Ganado Optimizado"))) {
                        jn.setEstadoGanado();
                    }
                }
            }
        }
    }

    /**
     *
     * @param pCantCajasMovimiento
     * @param pDeshacer
     * @throws java.lang.Exception
     */
    public static void cambiarReglas(int pCantCajasMovimiento, boolean pDeshacer) throws Exception {
        configuracion.setCajasPorMovimiento(pCantCajasMovimiento);
        configuracion.setPermitirDeshacer(pDeshacer);
    }

    /**
     *
     * @param pJuegoNivel
     * @throws Excepciones.ExcepcionNivel
     */
    public static void cambiarBestMoves(JuegoNivel pJuegoNivel) throws ExcepcionNivel {
        if (niveles.indexOf(pJuegoNivel.getNivel()) > -1) {
            niveles.get(niveles.indexOf(pJuegoNivel.getNivel())).setBestMoves(pJuegoNivel.getCantMovimientos());
            cambiarNivelesOptimizados(pJuegoNivel);
        }
    }
}
