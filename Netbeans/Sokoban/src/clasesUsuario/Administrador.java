/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesUsuario;

import Excepciones.ExcepcionUsuario;
import superClases.Usuario;
import java.awt.Image;

/**
 *Clase Administrador
 * Tipo de Usuario para configurar el juego
 * @author Administrador
 */
public class Administrador extends Usuario {
    //nacionalidad del usuario
    private String nacionalidad;
    //cantidad de niveles agregados
    private int cantNivelesAgregados;
    //cantidad de BestMoves que le han superado
    private int cantBestMovesSuperados;

    /**
     * Constructor de la clase Administrador
     * @param nacionalidad Nacionalidad
     * @param codUsuario Código unico del usuario
     * @param nombreCompleto Nombre completo del usuario
     * @param cedula Cédula del Administrador, con formato ######### 
     * @param eMail Correo electrónico del administrador
     * @param password Contraseña del Usuario
     * @param fotografia Fotografía del usuaro
     * @throws Excepciones.ExcepcionUsuario
     */
    public Administrador(String nacionalidad,
            String codUsuario, String nombreCompleto, String cedula, String eMail,
            String password, Image fotografia) throws ExcepcionUsuario {
        super(codUsuario, nombreCompleto, cedula, eMail, password, fotografia);
        this.nacionalidad = nacionalidad;
        this.cantNivelesAgregados = 0;
        this.cantBestMovesSuperados = 0;
    }

    /**
     *
     * @return
     */
    public String getNacionalidad() {
        return nacionalidad;
    }

    /**
     *
     * @param nacionalidad
     */
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    /**
     *
     * @return
     */
    public int getCantNivelesAgregados() {
        return cantNivelesAgregados;
    }

    /**
     *
     * @param cantNivelesAgregados
     * @throws Excepciones.ExcepcionUsuario
     */
    public void setCantNivelesAgregados(int cantNivelesAgregados) throws ExcepcionUsuario {
        if(cantNivelesAgregados < 0)
            throw new ExcepcionUsuario("La cantidad de niveles agregados no puede ser menor a 0");
        this.cantNivelesAgregados = cantNivelesAgregados;
    }

    /**
     *
     * @return
     */
    public int getCantBestMovesSuperados() {
        return cantBestMovesSuperados;
    }

    /**
     *
     * @param cantBestMovesSuperados
     * @throws Excepciones.ExcepcionUsuario
     */
    public void setCantBestMovesSuperados(int cantBestMovesSuperados) throws ExcepcionUsuario {
        if(cantBestMovesSuperados < 0)
            throw new ExcepcionUsuario("La cantidad de Best Moves superados no puede ser negativo");
        this.cantBestMovesSuperados = cantBestMovesSuperados;
    }

    @Override
    public String toString() {
        return "Administrador{" + "nacionalidad=" + nacionalidad + ", cantNivelesAgregados=" + cantNivelesAgregados + ", cantBestMovesSuperados=" + cantBestMovesSuperados + '}';
    }

}
