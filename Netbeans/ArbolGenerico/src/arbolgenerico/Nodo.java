package arbolgenerico;

import java.util.ArrayList;
import java.util.List;

public class Nodo<Tipo> {
    
    private Nodo<Tipo> padre;
    private Tipo valor;
    private List<Nodo<Tipo>> hijos;
    /**
     * Construye un nodo asígnado a un padre y con un valor inicial
     * @param padre
     * @param valor 
    */
    public Nodo(Nodo<Tipo> padre, Tipo valor) {
        this.padre = padre;
        this.valor = valor;
        hijos = new ArrayList<>();
    }
    //
    /**
     * Modifica el valor del nodo
     * @param valor 
    */
    public void setValor(Tipo valor) {
        this.valor = valor;
    }
    //
    /**
     * Devuelve el valor del nodo
     * @return 
    */
    public Tipo getValor() {
        return valor;
    }   
    //
    /**
     * Agrega un hijo
     * @param hijo 
    */
    public void agregarHijo(Nodo<Tipo> hijo) {
        hijos.add(hijo);
    } 
    //
    /**
     * Devuelve la lista de hijos
     * @return 
    */
    public List<Nodo<Tipo>> getHijos() {
        return this.hijos;
    }
    //
    /**
     * Indica si el nodo tiene hijos
     * @return falso si la lista de hijos está vacía
    */
    public boolean esPadre() {
        return !hijos.isEmpty();
    }
    //
    /**
     * Devuelve el nodo padre
     * @return 
    */
    public Nodo<Tipo> getPadre() {
        return this.padre;
    }
    //
    /**
     * Convierte el nodo a texto, el método se vuelve recursivo mientras haya nodos hijos
     * @return 
    */
    public String aTexto() {
        // Utilizo un StringBuilder para ir almacenando los valores y generando el texto que representa al nodo subárbol
        StringBuilder bString = new StringBuilder();
        bString.append(this.getValor());
        if (this.esPadre()) {
            bString.append("(");
            for (Nodo<Tipo> n: this.getHijos()) {
                bString.append(n.aTexto());
                bString.append(", ");
            }
            bString.delete(bString.length()-2, bString.length());
            bString.append(")");
        }
        return bString.toString();
    }
}
