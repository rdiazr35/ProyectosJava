package arbolgenerico;

public class Inicio {

    public static void main(String[] args) {
        // TODO code application logic here
        Arbol a;
        Nodo<String> raiz = new Nodo<>(null, "Pacientes");
        Nodo<String> g1Genero  = new Nodo<>(raiz, "Género");
        Nodo<String> g2H = new Nodo(g1Genero, "Hombre");
        g2H.getHijos().add(new Nodo<>(g2H,"Paciente1"));
        g2H.getHijos().add(new Nodo<>(g2H,"Paciente2"));                    
        g1Genero.agregarHijo(g2H);                
        Nodo<String> g2M = new Nodo(g1Genero, "Mujer");
        g2M.getHijos().add(new Nodo<>(g2M,"Paciente3"));
        g1Genero.agregarHijo(g2M);
        raiz.agregarHijo(g1Genero);
        
        Nodo<String> g1Sangre = new Nodo<>(raiz, "Sangre");
        Nodo<String> g2A = new Nodo<>(g1Sangre, "A");
        g2A.getHijos().add(new Nodo<>(g2A, "Paciente 1"));
        g1Sangre.agregarHijo(g2A);
        Nodo<String> g2B = new Nodo<>(g1Sangre, "B");
        g1Sangre.agregarHijo(g2B);
        Nodo<String> g2AB = new Nodo<>(g1Sangre, "AB");
        g1Sangre.agregarHijo(g2AB);
        Nodo<String> g2O = new Nodo<>(g1Sangre, "O");
        g2O.getHijos().add(new Nodo<>(g2O, "Paciente 2"));
        g2O.getHijos().add(new Nodo<>(g2O, "Paciente 3"));
        g1Sangre.agregarHijo(g2O);
        raiz.agregarHijo(g1Sangre);
        
        Nodo<String> g1Presion  = new Nodo<>(raiz, "Presión");
        Nodo<String> g2Alta = new Nodo<>(g1Presion, "Alta");
        g2Alta.getHijos().add(new Nodo<>(g2Alta, "Paciente 1"));
        g1Presion.agregarHijo(g2Alta);
        Nodo<String> g2Media = new Nodo<>(g1Presion, "Media");
        g2Media.getHijos().add(new Nodo<>(g2Media, "Paciente 2"));
        g1Presion.agregarHijo(g2Media);
        Nodo<String> g2Baja = new Nodo<>(g1Presion, "Baja");
        g2Baja.getHijos().add(new Nodo<>(g2Baja, "Paciente 3"));
        g1Presion.agregarHijo(g2Baja);
        raiz.agregarHijo(g1Presion);
        a = new Arbol(raiz);
        System.out.println(a.aTexto());
    }
}