package arbolgenerico;

/**
 *
 * @author Randy
 */
public class Arbol<Tipo> {
    Nodo<Tipo> raiz;
    
    /**
     * Constructor del árbol con raíz
     * @param raiz 
    */
    public Arbol(Nodo<Tipo> raiz) {
        this.raiz = raiz;
    }
    //
    /**
     * Convierte a texto el árbol utilizando el método aTexto() de Nodo<> un nodo puede ser un subárbol
     * @return 
     */
    public String aTexto() {
        return raiz.aTexto();
    }  
    //
}
